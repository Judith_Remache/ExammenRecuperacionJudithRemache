package edu.uoc.android.restservice.rest.contants;

public class ApiConstants {

    // BASE URL
    public static final String BASE_GITHUB_URL = "http://159.65.168.44:3003/profesores/";

    // ENDPOINTS
    public static final String GITHUB_USER_ENDPOINT = "profesor/{cedula}";
    public static final String GITHUB_FOLLOWERS_ENDPOINT = "/profesor/{cedula}";

    //http://localhost:3002/estudiante/1112223334
}

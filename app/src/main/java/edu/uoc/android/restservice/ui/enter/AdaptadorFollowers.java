package edu.uoc.android.restservice.ui.enter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Profesor;

/**
 * Created by edgardopanchana on 4/29/18.
 */

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    ArrayList<Profesor> listaProfesores;

  public AdaptadorFollowers(ArrayList<Profesor> listaFollowers) {
        this.listaProfesores = listaFollowers;
   }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
     holder.textViewNombres.setText(listaProfesores.get(position).getNombre());
      holder.textViewApellidos.setText(listaProfesores.get(position).getApellido());
    holder.textViewMateria.setText(listaProfesores.get(position).getMateria());
     holder.textViewTitulo.setText(listaProfesores.get(position).getTitulo());


    }

    @Override
    public int getItemCount() {
        return listaProfesores.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView textViewNombres, textViewApellidos, textViewTitulo, textViewCedula, textViewMateria;


        public ViewHolderFollowers(View itemView) {
            super(itemView);

            textViewNombres = (TextView) itemView.findViewById(R.id.textViewNombres);
            textViewApellidos = (TextView) itemView.findViewById(R.id.textViewApellidos);

            textViewTitulo = (TextView) itemView.findViewById(R.id.textViewTitulo);
            textViewCedula = (TextView) itemView.findViewById(R.id.textViewCedula);
            textViewMateria = (TextView) itemView.findViewById(R.id.textViewMateria);
        }
    }
}

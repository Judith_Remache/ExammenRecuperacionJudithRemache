package edu.uoc.android.restservice.ui.enter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Profesor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Profesor> listaProfesores;
    RecyclerView recyclerViewFollowers;

    TextView textViewNombre, textViewApellido, textViewTitulo, textViewMateria;
    ImageView imageViewProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewNombre = findViewById(R.id.textViewNombres);
        textViewApellido = findViewById(R.id.textViewApellidos);
        textViewTitulo = findViewById(R.id.textViewTitulo);
        textViewMateria = findViewById(R.id.textViewMateria);

        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        listaProfesores = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String cedula = getIntent().getStringExtra("cedula");
        mostrarDatosBasicos(cedula);

    }


    private void mostrarDatosBasicos(String cedula) {
        GitHubAdapter adapter = new GitHubAdapter();

        Call<Profesor> call = adapter.getOwner(cedula);

        call.enqueue(new Callback<Profesor>() {

            @Override
            public void onResponse(Call<Profesor> call, Response<Profesor> response) {
                Profesor owner = response.body();
                textViewNombre.setText(owner.getNombre().toString());
                textViewApellido.setText(owner.getApellido().toString());
                textViewTitulo.setText(owner.getTitulo().toString());
                textViewMateria.setText(owner.getMateria().toString());
                Picasso.get().load(owner.getImagen()).into(imageViewProfile);

            }

            @Override
            public void onFailure(Call<Profesor> call, Throwable t) {

            }
        });
    }}











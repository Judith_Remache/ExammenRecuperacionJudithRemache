package edu.uoc.android.restservice.rest.service;

import java.util.List;

import edu.uoc.android.restservice.rest.contants.ApiConstants;
import edu.uoc.android.restservice.rest.model.Profesor;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubService {

 @GET(ApiConstants.GITHUB_USER_ENDPOINT)  Call<Profesor> getOwner(@Path("cedula") String owner);

  @GET(ApiConstants.GITHUB_FOLLOWERS_ENDPOINT)
  Call<List<Profesor>> getOwnerFollowers(@Path("cedula") String owner);
}
